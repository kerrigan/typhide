function VideoCtrl($scope, $http, $rootScope) {
    var player = document.getElementById("player");
    function updateList(){
	$http.get("/videos").success(
	    function(data, status, headers, config){
		$scope.videos = data.playlist;
	    });
    }

    
    $scope.Math = window.Math;
    var ws = new WebSocket(
	"ws://" + window.location.hostname + ":"
	    + window.location.port + "/ws");
    ws.onopen = function(){
	console.log("broadcast started");
    };

    ws.onclose = function(){
	console.log("broadcast stopped");
    };

    ws.onmessage = function(message){
	var data = JSON.parse(message.data);
	console.log(data);
	$scope.$apply(function(){
	    $scope.title = data.title;
	    updateList();
	});

	if("id" in data){
	    player.loadVideoById(data.id);
	}

    };
    
    


    $http.get("/info")
	.success(function(data, status, headers, config){
	    var startTime = Math.round((new Date()).getTime() / 1000) - data.started;
	    player.loadVideoById(data.id, startTime);
	    $scope.title = data.title;
	})
	.error(function(data, status, headers, config){
	    console.log(status);
	});

    $http.get("/videos").success(
	function(data, status, headers, config){
	    $scope.videos = data.playlist;
	});


    $scope.addVideo = function(){
	var ytRegex = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
	var url = $scope.videoUrl;
	if(url != undefined){
	    var match = url.match(ytRegex);
	    if(match){
		var youtubeId = match[1];
		$http({url: "/videos",
		       data: {video: youtubeId},
		       headers: {'Content-Type':
				 'application/x-www-form-urlencoded'},
		       method: "POST"})
		    .success(function(data, status, headers, config){
			    $http.get("/videos").success(
				function(data, status, headers, config){
				    $scope.videos = data.playlist;
				    $scope.videoUrl = "";
				});
		    });
	    }
	}else
	    console.log("wrong url");
    };
}
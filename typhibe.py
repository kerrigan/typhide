# -*- coding: utf-8 -*-
#!/usr/bin/python
import datetime
import json
import math
import re
import os
import time

from tornado import gen
from tornado.escape import json_decode, json_encode
from tornado.httpclient import AsyncHTTPClient
from tornado.web import RequestHandler, Application, asynchronous, template
from tornado.websocket import WebSocketHandler
from tornado.ioloop import IOLoop

HOSTNAME = "jugregator.org"


COMMAND_REGEX = re.compile("((?P<command>[^\r\n]+)::(?P<argument>[^\r\n]+?)|(?P<logout>End))\r\n")


class Registry(object):
    playlist = []
    current = {"video": "uh-liQDE3cM",
               "started": 50,
               "duration": 124,
               "title": "some video"}

    @classmethod
    def post(self, description):
        if len(Registry.playlist) >= 10:
            return False
        
        Registry.playlist.append(description)
        if(len(Registry.playlist) == 1):
            Registry.current = Registry.playlist[0]
            Registry.current['started'] = math.floor(time.time())
            WSBroadcast.broadcast(Registry.current)
            IOLoop.instance().add_timeout(
                datetime.timedelta(seconds=Registry.current["duration"]),
                Registry.next_video)
        return True

    @classmethod
    def next_video(self):
        Registry.playlist = Registry.playlist[1:]
        if len(Registry.playlist) == 0:
            Registry.current = {}
            WSBroadcast.broadcast({})
            return
        
        Registry.current = Registry.playlist[0]
        Registry.current['started'] = math.floor(time.time())
        WSBroadcast.broadcast(Registry.current)
        IOLoop.instance().add_timeout(
            datetime.timedelta(seconds=Registry.current["duration"]),
            Registry.next_video)


class WSBroadcast(WebSocketHandler):

    clients = []


    @classmethod
    def broadcast(self, message):
        for client in WSBroadcast.clients:
            if client is not None:
                client.write_message(message)

    def open(self):
        WSBroadcast.clients.append(self)
        print "Connected"

    def on_close(self):
        WSBroadcast.clients.remove(self)
	print WSBroadcast.clients
        print "Closed"


class InfoHandler(RequestHandler):

    @asynchronous
    def get(self):
        self.write(Registry.current)
        self.finish()

class VideoListHandler(RequestHandler):

    @asynchronous
    def get(self):
        clips={"playlist": Registry.playlist}
        self.write(clips)
        self.finish()

    @gen.engine
    @asynchronous
    def post(self):
        request_data = json_decode(self.request.body)
        youtube_id = request_data.get("video")
        http_client = AsyncHTTPClient()
        video_info_response = yield gen.Task(
            http_client.fetch,
            'http://gdata.youtube.com/feeds/api/videos/%s?v=2&alt=jsonc' % youtube_id)
        js = json_decode(video_info_response.body)
        # import pdb;pdb.set_trace()
        video_info = {"title": js['data']['title'],
                      "duration": js['data']['duration'],
                      "id": youtube_id}
        if(Registry.post(video_info)):
            self.write({"result": "ok"})
        else:
            self.write({"result": "full"})
        self.finish()

class MainHandler(RequestHandler):
    
    @asynchronous
    def get(self):

        # client_logins = [value[1] for key, value in LineProtocol.clients.iteritems() if value[1] != ""]
        self.render("main.html")

application = Application([
        (r"/ws", WSBroadcast),
        (r"/info", InfoHandler),
        (r"/videos", VideoListHandler),
        (r"/", MainHandler),
        ],
                          debug=True,
                          static_path=os.path.join(os.path.dirname(__file__), "static"))


if __name__ == '__main__':
    application.listen(8888)
    #IOLoop.instance().add_timeout(datetime.timedelta(seconds=2), loop_ws)
    loop = IOLoop.instance()
    loop.start()
